# Assignment 1

## Description

Assignment 1 for a Mobile Applications Class

## Getting Started

### Dependencies

- Windows 10 and above
- Android Studio
- Git

### Installing

1. CLONE this repos into your Windows device
2. Open Project in Android Studio
3. Select the cloned repos folder

### Executing program

1. Open Project in Android Studio

## Help

N/A

## Authors

Keith Sialana

## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the MIT License - see the LICENSE.md file for details