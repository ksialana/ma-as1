package com.example.assignment1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class PlayerListAdapter extends ArrayAdapter<Player> {
    private static final String TAG = "PlayerListAdapter";
    private Context mContext;
    private int mResource;

    public PlayerListAdapter(Context context, int resource, ArrayList<Player> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String p_id = getItem(position).getP_id();
        String name = getItem(position).getName();
        int wins = getItem(position).getWins();
        int losses = getItem(position).getLosses();
        int ties = getItem(position).getTies();

        Player p = new Player(p_id,name,wins,losses,ties);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        TextView tvName = (TextView) convertView.findViewById(R.id.lblPlayer);
        TextView tvWins = (TextView) convertView.findViewById(R.id.lblWins);
        TextView tvLosses = (TextView) convertView.findViewById(R.id.lblLosses);
        TextView tvTies = (TextView) convertView.findViewById(R.id.lblTies);

        tvName.setText(name);
        tvWins.setText(String.valueOf(wins).toString());
        tvLosses.setText(String.valueOf(losses).toString());
        tvTies.setText(String.valueOf(ties).toString());

        return convertView;
    }
}
