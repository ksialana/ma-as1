/*
* PROGRAM:          TicTacToe
* FILE:             TicTacToeGame.java
* PROGRAMMER:       Keith Sialana
* DESCRIPTION:      Class file for the TicTacToe game
* CREATED:          Oct 13, 2021
* CHANGE HISTORY:
* */

package com.example.assignment1;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class TicTacToeGame {

    // Attributes
    static final List<List<Integer>> WinScenarios = new ArrayList<List<Integer>>() {{
        add(new ArrayList<Integer>(){{add(1);add(2);add(3);}});
        add(new ArrayList<Integer>(){{add(1);add(4);add(7);}});
        add(new ArrayList<Integer>(){{add(1);add(5);add(9);}});
        add(new ArrayList<Integer>(){{add(2);add(5);add(8);}});
        add(new ArrayList<Integer>(){{add(3);add(5);add(7);}});
        add(new ArrayList<Integer>(){{add(3);add(6);add(9);}});
        add(new ArrayList<Integer>(){{add(4);add(5);add(6);}});
        add(new ArrayList<Integer>(){{add(7);add(8);add(9);}});
    }};
    Context c;
    char activeCharacter;
    int activePlayer;
    int player1Score;
    int player2Score;
    int buttonsClicked;
    List<Tile> GameTiles;
    List<Integer> player1Tiles;
    List<Integer> player2Tiles;
    Character[] TileChars;
    Boolean gameEnd = false;
    FirebaseDatabase fb;
    DatabaseReference reference;


    // Methods
    public TicTacToeGame(Context c){
        TileChars = new Character[]{'X', 'O', ' '};
        activeCharacter = TileChars[0];
        activePlayer = 1;
        player1Score = 0;
        player2Score = 0;
        player1Tiles = new ArrayList<Integer>();
        player2Tiles = new ArrayList<Integer>();
        GameTiles = new ArrayList<Tile>(){{
            add(new Tile(1));
            add(new Tile(2));
            add(new Tile(3));
            add(new Tile(4));
            add(new Tile(5));
            add(new Tile(6));
            add(new Tile(7));
            add(new Tile(8));
            add(new Tile(9));
        }};
        TextView tv = (TextView)((Activity)c).findViewById(R.id.lblPlayer1);
        tv.setTextColor(Color.parseColor("#FFFF0000"));
        this.c = c;
    }

    public void ClickTile(Context con, int btnNumber, View view){
        // Function only works if the game is still going
        if(!gameEnd){

            // If the tile has already been clicked
            if (!GameTiles.get(btnNumber - 1).ChangeTile(activePlayer,activeCharacter)){
                Toast.makeText(con, "That tile is already taken", Toast.LENGTH_SHORT).show();
            }
            else {
                Button btn = (Button)view;
                btn.setText(GameTiles.get(btnNumber-1).tileText.toString());

                // Adds tile number to player tiles
                switch (activePlayer){
                    case 1:{
                        player1Tiles.add(btnNumber);
                        break;
                    }
                    case 2:{
                        player2Tiles.add(btnNumber);
                        break;
                    }
                }
                buttonsClicked++;

                // Checks if the player won
                if(CheckTilesForWin()){
                    gameEnd = true;
                    if (activePlayer == 1)
                        Toast.makeText(con, ActivePlayers.player1.getName()+" has won!",
                                Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(con, ActivePlayers.player2.getName()+" has won!",
                                Toast.LENGTH_SHORT).show();
                    switch (activePlayer){
                        case 1:
                        {
                            ActivePlayers.player1.setWins(
                                    ActivePlayers.player1.getWins() + 1
                            );
                            ActivePlayers.player2.setLosses(
                                    ActivePlayers.player2.getLosses() + 1
                            );
                            TextView lbl = (TextView) ((Activity)con).findViewById(
                                    R.id.lblPlayer1Score);
                            lbl.setText(Integer.toString(++player1Score));
                            break;
                        }
                        case 2:
                        {
                            ActivePlayers.player2.setWins(
                                    ActivePlayers.player2.getWins() + 1
                            );
                            ActivePlayers.player1.setLosses(
                                    ActivePlayers.player1.getLosses() + 1
                            );
                            TextView lbl = (TextView) ((Activity)con).findViewById(
                                    R.id.lblPlayer2Score);
                            lbl.setText(Integer.toString(++player2Score));
                            break;
                        }
                    }

                    // Update Database
                    fb = FirebaseDatabase.getInstance();
                    DatabaseReference dbReference;
                    dbReference = fb.getReference().child("Player");

                    dbReference.child(ActivePlayers.player1.getP_id())
                            .setValue(ActivePlayers.player1);
                    dbReference.child(ActivePlayers.player2.getP_id())
                            .setValue(ActivePlayers.player2);

                } else if (buttonsClicked == 9 && !CheckTilesForWin()) {
                    gameEnd = true;
                    Toast.makeText(c, "It's a tie!", Toast.LENGTH_SHORT).show();
                    ActivePlayers.player1.setTies(
                            ActivePlayers.player1.getTies() + 1
                    );
                    ActivePlayers.player2.setTies(
                            ActivePlayers.player2.getTies() + 1
                    );

                    // Update Database
                    fb = FirebaseDatabase.getInstance();
                    DatabaseReference dbReference;
                    dbReference = fb.getReference().child("Player");

                    dbReference.child(ActivePlayers.player1.getP_id())
                            .setValue(ActivePlayers.player1);
                    dbReference.child(ActivePlayers.player2.getP_id())
                            .setValue(ActivePlayers.player2);
                }

                // Changes active player
                if (activePlayer == 1){
                    activePlayer = 2;
                    activeCharacter = TileChars[1];
                    TextView tv = (TextView)((Activity)c).findViewById(R.id.lblPlayer1);
                    tv.setTextColor(Color.parseColor("#FF3A3A3A"));
                    tv = (TextView)((Activity)c).findViewById(R.id.lblPlayer2);
                    tv.setTextColor(Color.parseColor("#FFFF0000"));
                }else{
                    activePlayer = 1;
                    activeCharacter = TileChars[0];
                    TextView tv = (TextView)((Activity)c).findViewById(R.id.lblPlayer2);
                    tv.setTextColor(Color.parseColor("#FF3A3A3A"));
                    tv = (TextView)((Activity)c).findViewById(R.id.lblPlayer1);
                    tv.setTextColor(Color.parseColor("#FFFF0000"));
                }
            }
        }
        else{
            Toast.makeText(con, "The Game has ended, click New Game",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public boolean CheckTilesForWin(){
        List<Integer> activePlayerTileList;

        switch(activePlayer){
            case 1:
                activePlayerTileList = player1Tiles;
                break;
            case 2:
                activePlayerTileList = player2Tiles;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + activePlayer);
        }

        for (List<Integer> e :WinScenarios) {
            Boolean confirmPattern = true;

            // Goes through each number in the selected WinScenario
            for (Integer i : e) {
                // Changes WinScenario if number isn't detected in the Tile List
                if (!activePlayerTileList.contains(i)){
                    confirmPattern = false;
                    break;
                }
            }
            if (confirmPattern){
                return true;
            }
        }
        return false;
    }

    public void RestartGame(){
        player1Tiles.clear();
        player2Tiles.clear();
        GameTiles.clear();
        GameTiles = new ArrayList<Tile>(){{
            add(new Tile(1));
            add(new Tile(2));
            add(new Tile(3));
            add(new Tile(4));
            add(new Tile(5));
            add(new Tile(6));
            add(new Tile(7));
            add(new Tile(8));
            add(new Tile(9));
        }};
        gameEnd = false;
        List<Button> buttons = new ArrayList<Button>(){{
            add((Button)((Activity)c).findViewById(R.id.btnTL));
            add((Button)((Activity)c).findViewById(R.id.btnTC));
            add((Button)((Activity)c).findViewById(R.id.btnTR));
            add((Button)((Activity)c).findViewById(R.id.btnML));
            add((Button)((Activity)c).findViewById(R.id.btnMC));
            add((Button)((Activity)c).findViewById(R.id.btnMR));
            add((Button)((Activity)c).findViewById(R.id.btnBL));
            add((Button)((Activity)c).findViewById(R.id.btnBC));
            add((Button)((Activity)c).findViewById(R.id.btnBR));
        }};
        for (Button b: buttons) {
            b.setText(TileChars[2].toString());
        }
        buttonsClicked = 0;
    }
}
