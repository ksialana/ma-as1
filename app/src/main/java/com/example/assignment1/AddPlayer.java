package com.example.assignment1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddPlayer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_player);
    }

    public void btnAdd_OnClick(View view) {
        FirebaseDatabase db;
        db = FirebaseDatabase.getInstance();
        DatabaseReference dbReference;
        dbReference = db.getReference().child("Player");

        EditText playerName = this.findViewById(R.id.tbPlayerName);

        Player player = new Player(playerName.getText().toString(), 0,0,0);
        dbReference.child(player.getP_id()).setValue(player);
        playerName.setText("");

        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Toast.makeText(AddPlayer.this,player.getName() +
                        " has been added!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(AddPlayer.this,player.getName() +
                        " could not be added",Toast.LENGTH_SHORT).show();
            }
        });
    }
}