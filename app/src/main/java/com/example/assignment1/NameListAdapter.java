package com.example.assignment1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class NameListAdapter extends ArrayAdapter<Player> {
    private static final String TAG = "NameListAdapter";
    private Context mContext;
    private int mResource;

    public NameListAdapter(Context context, int resource, ArrayList<Player> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String p_id = getItem(position).getP_id();
        String name = getItem(position).getName();
        int wins = getItem(position).getWins();
        int losses = getItem(position).getLosses();
        int ties = getItem(position).getTies();

        Player p = new Player(p_id,name,wins,losses,ties);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        TextView tvName = (TextView) convertView.findViewById(R.id.pnl_tvName);

        tvName.setText(name);

        return convertView;
    }

}
