/*
 * PROGRAM:          TicTacToe
 * FILE:             Tile.java
 * PROGRAMMER:       Keith Sialana
 * DESCRIPTION:      Class file for an instance of a Tile within the TicTacToe Game
 * CREATED:          Oct 14, 2021
 * CHANGE HISTORY:
 * */
package com.example.assignment1;

public class Tile {
    int tileNumber;
    Boolean isEmpty = true;
    Character tileText;
    int player;

    public Tile(int i) {
        tileNumber = i;
        tileText = ' ';
    }

    public Boolean ChangeTile(int player, Character tileText){
        if (isEmpty){
            this.tileText = tileText;
            this.player = player;
            isEmpty = false;
            return true;
        }
        return false;
    }

    public void ClearTile(){
        tileText = ' ';
        player = Integer.parseInt(null);
        isEmpty = true;
    }
}
