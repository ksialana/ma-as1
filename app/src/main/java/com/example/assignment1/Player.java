package com.example.assignment1;
import java.math.BigInteger;
import java.security.MessageDigest;

public class Player {
    private String p_id;
    private String name;
    private int wins;
    private int losses;
    private int ties;

    public Player(){

    }

    public Player(String p_id, String name, int wins, int losses, int ties){
        this.p_id = p_id;
        this.name = name;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
    }

    public Player(String name, int wins, int losses, int ties) {
        String sha1 = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(name.getBytes("utf8"));
            sha1 = String.format("%040x", new BigInteger(1, digest.digest()));
        } catch (Exception e){
            e.printStackTrace();
        }
        if (sha1 == "")
            return;
        this.p_id = sha1;
        this.name = name;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
    }

    public String getP_id() {
        return p_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getTies() {
        return ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }
}
