package com.example.assignment1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ScoreBoard extends AppCompatActivity {

    private ListView listview;
    private ArrayList<Player> playerList;
    PlayerListAdapter arrayAdapter;
    FirebaseDatabase fb;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_board);

        listview = (ListView) findViewById(R.id.lvPlayers);
        fb = FirebaseDatabase.getInstance();
        reference = fb.getReference("Player");
        playerList = new ArrayList<Player>();

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Player value = snapshot.getValue(Player.class);
                playerList.add(value);
                Collections.sort(playerList, new Comparator<Player>(){
                    public int compare(Player p1, Player p2){
                        return Integer.valueOf(p2.getWins()).compareTo(p1.getWins());
                    }
                });
                arrayAdapter = new PlayerListAdapter(ScoreBoard.this, R.layout.adapter_view_layout, playerList);
                listview.setAdapter(arrayAdapter);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}