package com.example.assignment1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.NameList;

import java.util.ArrayList;

public class SelectPlayer extends AppCompatActivity {

    private ListView listview;
    private ArrayList<Player> playerList;
    NameListAdapter arrayAdapter;
    FirebaseDatabase fb;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_player);

        listview = (ListView) findViewById(R.id.list_name);
        fb = FirebaseDatabase.getInstance();
        reference = fb.getReference("Player");
        playerList = new ArrayList<Player>();

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Player value = snapshot.getValue(Player.class);
                playerList.add(value);
                arrayAdapter = new NameListAdapter(SelectPlayer.this,
                        R.layout.player_name_list, playerList);
                listview.setAdapter(arrayAdapter);

                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Player p = (Player) parent.getItemAtPosition(position);
                        if (ActivePlayers.playerChangeRequest == 1){
                            if(ActivePlayers.player2 != null &&
                                    ActivePlayers.player2.getP_id() == p.getP_id())
                                Toast.makeText(SelectPlayer.this,
                                        "You can't choose the same name as the other player",
                                        Toast.LENGTH_SHORT).show();
                            else{
                                ActivePlayers.player1 = p;
                                startActivity(new Intent(SelectPlayer.this,
                                        MainActivity.class));
                            }
                        } else if (ActivePlayers.playerChangeRequest == 2){
                            if(ActivePlayers.player1 != null &&
                                    ActivePlayers.player1.getP_id() == p.getP_id())
                                Toast.makeText(SelectPlayer.this,
                                        "You can't choose the same name as the other player",
                                        Toast.LENGTH_SHORT).show();
                            else{
                                ActivePlayers.player2 = p;
                                startActivity(new Intent(SelectPlayer.this,
                                        MainActivity.class));
                            }
                        }
                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}