package com.example.assignment1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnStartGame_OnClick(View view) {
        if(ActivePlayers.player1 == null){
            ActivePlayers.playerChangeRequest = 1;
            Toast.makeText(this, "Player 1, pick a name to play with",
                    Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, SelectPlayer.class));
        } else if (ActivePlayers.player2 == null){
            ActivePlayers.playerChangeRequest = 2;
            Toast.makeText(this, "Player 2, pick a name to play with",
                    Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, SelectPlayer.class));
        }else{
            startActivity(new Intent(this,TicTacToeActivity.class));
        }
    }

    public void btnScoreboard_OnClick(View view) {
        startActivity(new Intent(this,ScoreBoard.class));
    }

    public void btnAddPlayer_OnClick(View view) {
        startActivity(new Intent(this,AddPlayer.class));
    }

    public void btnSelectPlayer(View view) {
        if (this.findViewById(view.getId()) == this.findViewById(R.id.btnSelectP1)){
            ActivePlayers.playerChangeRequest = 1;
        }
        else if (this.findViewById(view.getId()) == this.findViewById(R.id.btnSelectP2)){
            ActivePlayers.playerChangeRequest = 2;
        }
        startActivity(new Intent(this, SelectPlayer.class));
    }
}