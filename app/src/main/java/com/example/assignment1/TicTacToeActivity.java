/*
 * PROGRAM:          TicTacToe
 * FILE:             MainActivity.java
 * PROGRAMMER:       Keith Sialana
 * DESCRIPTION:      Main Activity file that acts as the housing area of where all the widgets and
 *                      Views are placed
 * CREATED:          Oct 13, 2021
 * CHANGE HISTORY:
 * */
package com.example.assignment1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class TicTacToeActivity extends AppCompatActivity {

    TicTacToeGame TTTGame;
    TextView lblPlayer1;
    TextView lblPlayer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        TTTGame = new TicTacToeGame(this);
        lblPlayer1 = this.findViewById(R.id.lblPlayer1);
        lblPlayer2 = this.findViewById(R.id.lblPlayer2);

        lblPlayer1.setText(ActivePlayers.player1.getName() + " ( X )");
        lblPlayer2.setText(ActivePlayers.player2.getName() + " ( O )");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.optNewGame){
            TTTGame.RestartGame();
        }
        return super.onOptionsItemSelected(item);
    }

    public void btnClickHandler(View view) {
        switch (view.getId()){
            case R.id.btnTL:{
                TTTGame.ClickTile(this, 1,view);
                break;
            }
            case R.id.btnTC:{
                TTTGame.ClickTile(this, 2,view);
                break;
            }
            case R.id.btnTR:{
                TTTGame.ClickTile(this, 3,view);
                break;
            }
            case R.id.btnML:{
                TTTGame.ClickTile(this, 4,view);
                break;
            }
            case R.id.btnMC:{
                TTTGame.ClickTile(this, 5,view);
                break;
            }
            case R.id.btnMR:{
                TTTGame.ClickTile(this, 6,view);
                break;
            }
            case R.id.btnBL:{
                TTTGame.ClickTile(this, 7,view);
                break;
            }
            case R.id.btnBC:{
                TTTGame.ClickTile(this, 8,view);
                break;
            }
            case R.id.btnBR:{
                TTTGame.ClickTile(this, 9,view);
                break;
            }

        }
    }
}